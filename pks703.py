#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2015, 2021 Mark J. Blair, NF6X
#
# This file is part of pks703.
#
#  pks703 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pks703 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pks703.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Utility for reading and writing KSD-64 compatible keys with a Datakey
PKS703 parallel key reader/writer device."""

version   = '2.0'
copyright = 'Copyright (C) 2015, 2021 Mark J. Blair, released under GPLv3'
url       = 'https://gitlab.com/NF6X_Crypto/pks703'

import argparse
import os
import serial
import string
import sys
import time


################################################################
# Exceptions that may be raised by classes in this module
################################################################

class PKS703Exception(Exception):
    """Base class for all other exceptions defined in this module."""
    pass

class PksTimeout(PKS703Exception):
    """Raised when timeout occurs during communications with PKS703."""
    def __init__(self,
                 msg='Timeout occurred while communicating with PKS703.'):
        PKS703Exception.__init__(self, msg)

class PksBadResponse(PKS703Exception):
    """Raised when PKS703 responds with unexpected characters."""
    def __init__(self, msg='PKS703 response did not make sense.'):
        PKS703Exception.__init__(self, msg)

class PksKeyNotPresent(PKS703Exception):
    """Raised when a command fails because no key is present."""
    def __init__(self, msg='Key is not inserted.'):
        PKS703Exception.__init__(self, msg)

class PksChecksum(PKS703Exception):
    """Raised when a bad checksum is received.

    Pass this exception two arguments as byte strings of length 1:
    Expected value, received value."""
    def __init__(self, expected, received):
        PKS703Exception.__init__(self,
                                 'Bad checksum reseived:'\
                                 ' Expected 0x{:02x}, received 0x{:02x}.'\
                                 .format(expected[0], received[0]))

class PksStatus(PKS703Exception):
    """Raised when unsuccessful status is returned by PKS703."""
    pass

class PksInvalidFileSize(PKS703Exception):
    """Raised when trying to read/write data with size not matching key length.

    Pass this exception two arguments:
    required data size, actual data size."""

    def __init__(self, size_needed, size_found):
        msg = 'Data size must be {:d} bytes; {:d} bytes found.'\
                .format(size_needed, size_found)
        self.size_needed = size_needed
        self.size_found  = size_found
        PKS703Exception.__init__(self, msg)

    
################################################################
# Class for representing key data in memory
################################################################

class KeyData(bytearray):
    """In-memory representation of key data."""

    def hex_dump(self):
        """Return hexadecimal dump of key data."""
        outbuf = ''
        hexbuf = ''
        ascbuf = ''
    
        for n in range(len(self)):
            hexbuf = hexbuf + ' {:02X}'.format(self[n])
            if chr(self[n]) in (string.ascii_letters \
                                + string.digits \
                                + string.punctuation \
                                + ' '):
                ascbuf = ascbuf + chr(self[n])
            else:
                ascbuf = ascbuf + '.'

            if n % 16 == 15:
                outbuf = outbuf \
                    + '{:04X}: '.format(n-15) \
                    + hexbuf \
                    + '    ' \
                    + ascbuf \
                    + '\n'
                hexbuf = ''
                ascbuf = ''

        return outbuf


    
################################################################
# Class for interacting with PKS703 reader/writer
################################################################

class PKS703:

    # Misc. constants for PKS703
    _DEV_ADDR     = b'\x00'
    _ETX          = b'\xFF'
    _STAT_RSP     = b'\x06'
    _DATA_RSP     = b'\x07'
    _READ_TIMEOUT = 5

    # Status request modifiers
    _KEY_STATUS_IMMEDIATE       = b'\x00'
    _KEY_STATUS_KEY_PRESENT     = b'\x01'
    _KEY_STATUS_KEY_NOT_PRESENT = b'\x02'
    _KEY_STATUS_KEY_CHANGE      = b'\x03'

    # Command codes
    _CMD_READ       = b'\x15'
    _CMD_WRITE      = b'\x16'
    _CMD_INITIALIZE = b'\x17'
    _CMD_STATUS     = b'\x06'
    _CMD_KEYSIZE    = b'\x18'

    # Command status codes
    _STATUS_SUCCESS          = b'\x00'
    _STATUS_KEY_NOT_PRESENT  = b'\x01'
    _STATUS_INVALID_COMMAND  = b'\x02'
    _STATUS_KEY_REMOVED      = b'\x04'
    _STATUS_INVALID_DEV_ADDR = b'\x0A'
    _STATUS_TRANS_ERR        = b'\x0B'
    _STATUS_MEM_WRITE_ERR    = b'\x10'
    _STATUS_ADDR_RANGE_ERR   = b'\x11'

    # Dictionary of status strings as defined in PKS703 manual
    _status_dict = {_STATUS_SUCCESS:          'Key is in or request is'\
                                              ' successful.',
                    _STATUS_KEY_NOT_PRESENT:  'Key is not present.',
                    _STATUS_INVALID_COMMAND:  'Invalid/incomplete command'\
                                              ' issued (syntax error, bad'\
                                              ' address).',
                    _STATUS_KEY_REMOVED:      'Key was removed while writing'\
                                              ' to the key.',
                    _STATUS_INVALID_DEV_ADDR: 'Invalid device address.',
                    _STATUS_TRANS_ERR:        'Transmission error (checksum'\
                                              ' error).',
                    _STATUS_MEM_WRITE_ERR:    'Memory write error / Key'\
                                              ' contact is incomplete.',
                    _STATUS_ADDR_RANGE_ERR:   'Address range error.'}


    
    def __init__(self, port, baud=9600, verbose=False):
        self._port = serial.Serial(port=port, baudrate=baud,
                                   timeout=self._READ_TIMEOUT, rtscts=True)
        self.verbose = verbose

    def _checksum(self, data):
        """Private helper: calculate checksum of data."""
        cs = 0
        for dbyte in data:
            cs = cs + dbyte
            if cs > 0xFF:
                cs = (cs + 1) & 0xFF
        cs = cs & 0x7F
        return bytes([cs])
    

    def _send_command(self, cmd):
        """Private helper: Send command."""
        self._port.write(self._DEV_ADDR + cmd + self._ETX)

    def _get_response(self, data_len):
        """Private helper: Get response.

        Reader protocol does not permit robust reads of arbitrary data
        lengths due to ambiguity of escaped 0xFF vs. ETX followed by
        checksum (which can be 0xFF 1/256 of the time). So, we will
        expect the caller to specify the data length to be read.

        Keyword arguments:
        data_len -- Length of expected data.
                    If 0, expect a status response and return the
                    status code rather than raising an exception.
        """

        assert data_len >= 0
        
        state  = 0
        nstate = 0
        n      = 0
        data   = b'' # Decoded data
        rcvd   = b'' # Raw characters received; include in checksum
        status = None

        statelist = ['Device Address', 'Response Type', 'ETX Prior to Data',
                     'Data', 'ETX Following Data', 'Checksum', 'Status Code',
                     'ETX Following Status Code']
        while True:
            rsp = self._port.read(1)

            if len(rsp) != 1:
                raise PksTimeout('Timeout while reading {:s}' \
                                 .format(statelist[state]))
            else:
                if state == 0:
                    # Expecting device address byte
                    if rsp == self._DEV_ADDR:
                        nstate = 1
                    else:
                        raise PksBadResponse('Expected 0x{:02X}, got 0x{:02X}.'\
                                             .format(self._DEV_ADDR[0], rsp[0]))
                elif state == 1:
                    # Expecting response type byte
                    if rsp == self._DATA_RSP:
                        # Data should follow
                        if data_len > 0:
                            nstate = 2
                        else:
                            # Expected status
                            raise PksBadResponse('Expected status response;'\
                                                 ' got data response.')
                    elif rsp == self._STAT_RSP:
                        # Status should follow
                        nstate = 6
                    else:
                        raise PksBadResponse('Expected status or data response;'\
                                             ' got  0x{:02X}.'.format(rsp[0]))
                elif state == 2:
                    # Expecting ETX prior to data
                    if rsp == self._ETX:
                        nstate = 3
                    else:
                        raise PksBadResponse('Expected ETX prior to data;'\
                                             ' got  0x{:02X}.'.format(rsp[0]))
                elif state == 3:
                    # Reading data from reader
                    rcvd = rcvd + rsp
                    if rsp == self._ETX:
                        # 0xFF must be doubled
                        rsp = self._port.read(1)
                        rcvd = rcvd + rsp
                        if len(rsp) != 1:
                            raise PksTimeout('Timeout reading second 0xFF:'\
                                             ' state={:d} n={:d}'.format(state, n))
                        elif rsp != self._ETX:
                            raise PksBadResponse('Expected second 0xFF;'\
                                                 ' got 0x{:02x}.'.format(rsp[0]))
                    data = data + rsp
                    n = n + 1
                    if n >= data_len:
                        nstate = 4
                elif state == 4:
                    # ETX following data
                    if rsp == self._ETX:
                        nstate = 5
                    else:
                        raise PksBadResponse('Expected ETX after data;'\
                                             ' got  0x{:02X}.'.format(rsp[0]))
                elif state == 5:
                    # Checksum
                    cs = self._checksum(rcvd)
                    if cs == rsp:
                        # Good checksum
                        return data
                    else:
                        raise PksChecksum(cs, rsp)
                elif state == 6:
                    # Status Code
                    status = rsp
                    nstate = 7
                elif state == 7:
                    if data_len == 0:
                        # Caller wants the status code to be returned
                        return status
                    elif status == self._STATUS_KEY_NOT_PRESENT:
                        raise PksKeyNotPresent()
                    elif status in self._status_dict:
                        raise PksStatus(self._status_dict[rsp])
                    else:
                        raise PksBadData('Unknown status byte 0x{:02X}'\
                                         ' received from PKS703'.format(status[0]))
                    
            state = nstate

        

    def read(self):
        """Read key contents."""
        keylen = self.key_size()
        start_addr = b'\x00\x00'
        end_addr   = bytes([(keylen-1) >> 8, (keylen-1) & 0xFF])

        self._send_command(self._CMD_READ + start_addr + end_addr)

        if self.verbose:
            sys.stdout.write('Receiving data from PKS703...')
            sys.stdout.flush()

        data = self._get_response(keylen) 

        if self.verbose:
            sys.stdout.write(' read {:d} bytes.\n'.format(len(data)))
            sys.stdout.flush()

        key = KeyData(data)
        return key

    def write(self, data):
        """Write data to key. Data size must match size of inserted key."""
        keylen = self.key_size()
        if len(data) != keylen:
            raise PksInvalidFileSize(keylen, len(data))

        start_addr = b'\x00\x00'
        end_addr   = bytes([(keylen-1) >> 8, (keylen-1) & 0xFF])

        if self.verbose:
            sys.stdout.write('Sending data to PKS703...')
            sys.stdout.flush()

        self._send_command(self._CMD_WRITE + start_addr + end_addr)

        sent = b'' # Data sent; include in checksum

        for n in range(keylen):
            self._port.write(data[n:n+1])
            sent = sent + data[n:n+1]
            if data[n:n+1] == self._ETX:
                # Double any 0xFF bytes
                self._port.write(data[n:n+1])
                sent = sent + data[n:n+1]

        self._port.write(self._ETX)
        sent = sent + self._ETX
        cs = self._checksum(sent)
        self._port.write(cs)

        if self.verbose:
            sys.stdout.write(' sent {:d} bytes.\n'.format(len(data)))
            sys.stdout.write('Waiting for status from PKS703...')
            sys.stdout.flush()

        while (self._port.inWaiting() == 0):
            time.sleep(1)
            if self.verbose:
                sys.stdout.write('.')
                sys.stdout.flush()
        if self.verbose:
            sys.stdout.write('\n')
            
        status = self._get_response(0)
        assert len(status) == 1
        if status == self._STATUS_SUCCESS:
            return
        elif status == self._STATUS_KEY_NOT_PRESENT:
            raise PksKeyNotPresent()
        elif status in self._status_dict:
            raise PksStatus(self._status_dict[status])
        else:
            raise PksBadData('Unknown status byte 0x{:02X} received'\
                             ' from PKS703'.format(status[0]))

    def initialize(self):
        """Initialize the key to 0xFF and return key size."""
        if self.verbose:
            sys.stdout.write('Erasing key...')
            sys.stdout.flush()
        self._send_command(self._CMD_INITIALIZE)
        rsp = self._get_response(1)
        assert len(rsp) == 1
        key_size = 1024 * rsp[0]
        if self.verbose:
            sys.stdout.write(' {:d} bytes erased.\n'.format(key_size))
            sys.stdout.flush()
        return key_size
        

    def status(self):
        """Get status from key reader.

        Not yet implemented."""
        raise NotImplementedError()

    def key_size(self):
        """Get size of inserted key from key reader."""
        self._send_command(self._CMD_KEYSIZE)
        rsp = self._get_response(1)
        assert len(rsp) == 1
        key_size = 1024 * rsp[0]
        return key_size


################################################################
# Subcommands
################################################################

def cmd_read(args):
    """Read key and save to file."""
    pks703 = PKS703(port=args.port, baud=args.baud, verbose=args.verbose)
    data   = pks703.read()
    args.file.write(data)

def cmd_write(args):
    """Write file to key."""
    pks703 = PKS703(port=args.port, baud=args.baud, verbose=args.verbose)
    data   = args.file.read()
    try:
        pks703.write(data)
    except PksInvalidFileSize as e:
        print('ERROR: File size ({:d}) does not match key size ({:d})'\
              .format(e.size_found, e.size_needed),
              file=sys.stderr)
        exit(1)

def cmd_erase(args):
    """Erase the key to all 0xFF."""
    pks703 = PKS703(port=args.port, baud=args.baud, verbose=args.verbose)
    pks703.initialize()

def cmd_dump(args):
    """Hexadecimal dump of key"""
    pks703 = PKS703(port=args.port, baud=args.baud, verbose=args.verbose)
    data   = pks703.read()
    if args.file is not None:
        args.file.write(data.hex_dump())
    else:
        print(data.hex_dump())

def cmd_size(args):
    """Print size of key, in bytes."""
    pks703 = PKS703(port=args.port, baud=args.baud, verbose=args.verbose)
    print(pks703.key_size())


################################################################
# Main entry point
################################################################

if __name__ == '__main__':

    # Parser for command-line arguments
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('-p', '--port', metavar='PORT',
                        default='/dev/psk703',
                        help='serial port for PKS703 [/dev/psk703]')

    parser.add_argument('-b', '--baud', metavar='BAUD', type=int,
                        default=9600,
                        help='PKS703 baud rate [9600]')

    parser.add_argument('-v', '--verbose', action='store_true',
                        help='print status messages during PKS703 operations')

    subparsers = parser.add_subparsers(title='commands', metavar='COMMAND',
                                       help='PKS703 operation')

    # read subcommand
    parser_read = subparsers.add_parser('read',
                                        help='read key and save to file')
    parser_read.add_argument('file', metavar='FILE',
                             type=argparse.FileType('wb'),
                             help='file to be created')
    parser_read.set_defaults(func=cmd_read)

    # write subcommand
    parser_write = subparsers.add_parser('write',
                                        help='write file contents to key')
    parser_write.add_argument('file', metavar='FILE',
                             type=argparse.FileType('rb'),
                             help='file to be written to key')
    parser_write.set_defaults(func=cmd_write)


    # erase subcommand
    parser_erase = subparsers.add_parser('erase',
                                         help='erase key to all 0xFF state')
    parser_erase.set_defaults(func=cmd_erase)

    # dump subcommand
    parser_dump = subparsers.add_parser('dump',
                                        help='hex dump of key to stdout or file')
    parser_dump.add_argument('file', metavar='FILE', nargs='?',
                             type=argparse.FileType('w'),
                             help='send hex dump to file instead of stdout')
    parser_dump.set_defaults(func=cmd_dump)

    # size subcommand
    parser_size = subparsers.add_parser('size',
                                        help='print size of key in bytes')
    parser_size.set_defaults(func=cmd_size)


    # Parse the command line and execute the subcommand
    args = parser.parse_args()
    if 'func' not in args:
        parser.error('You need to specify a command.')
    else:
        try:
            args.func(args)
        except PksKeyNotPresent:
            print('ERROR: No key inserted in PKS703.', file=sys.stderr)
            sys.exit(1)
        

