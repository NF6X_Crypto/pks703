#!/usr/bin/env python3
#
##########################################################################
# Copyright (C) 2015, 2021 Mark J. Blair, NF6X
#
# This file is part of pks703.
#
#  pks703 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pks703 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pks703.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Installation script for pks703.

Usage examples:

    Install in your user directory:
        ./setup.py install --user

    Install in the system default location:
        sudo ./setup.py install

    Install under /usr/local:
        sudo ./setup.py install --prefix /usr/local

    Create a source distribution:
        ./setup.py sdist

"""


from distutils.core import setup

setup(name          = 'pks703',
      version       = '2.0',
      description   = 'Utility for reading and writing KSD-64 compatible' \
                      ' keys with a Datakey PKS703 parallel key' \
                      ' reader/writer device.',
      author        = 'Mark J. Blair',
      author_email  = 'nf6x@nf6x.net',
      url           = 'https://gitlab.com/NF6X_Crypto/pks703',
      download_url  = 'https://gitlab.com/NF6X_Crypto/pks703',
      license       = 'GPLv3',
      packages      = [],
      scripts       = ['pks703.py'])

