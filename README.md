# Datakey PKS703 Utility

## INTRODUCTION

`pks703.py` is a utility for reading and writing KSD-64 compatible keys with a
Datakey PKS703 parallel key reader/writer device.


## INSTALLATION

Use the `setup.py` script to install pks703. Here are some usage examples:

Install in your user directory:

    ./setup.py install --user

Install in the system default location:

    sudo ./setup.py install

Install under /usr/local:

    sudo ./setup.py install --prefix /usr/local

Create a source distribution:

    ./setup.py sdist


## USAGE EXAMPLES

You will need to use the `-p` flag to specify the PKS703 serial port unless you create a symlink `/dev/pks703` to point to the serial port. Baud rate defaults to 9600, and may be changed with the `-b` flag. Even/odd parity is not supported.

Read key contents and save them to file `foo.dat`:

    pks703.py -p /dev/ttyS0 read foo.dat

Write file `foo.dat` to key:

    pks703.py -p /dev/ttyS0 write foo.dat

Print hexadecimal dump of key contents:

    pks703.py -p /dev/ttyS0 dump

Save hexadecimal dump of key contents to file `foo.txt`:

    pks703.py -p /dev/ttyS0 dump foo.txt

Erase the key:

    pks703.py -p /dev/ttyS0 erase

Print the size of the inserted key:

    pks703.py -p /dev/ttyS0 size


## AUTHOR

Mark J. Blair, NF6X <nf6x@nf6x.net>


## COPYRIGHT

pks703 is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

pks703 is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
pks703.  If not, see <http://www.gnu.org/licenses/>.
